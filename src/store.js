import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
  	one_player: "02:00",
  	two_player: "02:00"
  },
  mutations: {
  	loadStatePLayers(state, payload) {
      if (payload.player === 'oneplayer') { state.one_player = payload.res }
      if (payload.player === 'twoplayer') {state.two_player = payload.res}
  	}
  },
  actions: {

  }
})
